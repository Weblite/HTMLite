# Lightweight HyperText Markup Language
This specification defines HyperText Markup Language as implemented in the broader diversity of web browsers, including Rhapsode, Lynx, Dillo, Netsurf, Weasyprint, etc.
HTML is a language for annotating plain text with its semantic structure, and to reference related resources.
HTML specifically does not dictate how its text should be presented.
For the sake of rendering to a variety of devices, and to ease website authoring & maintenance.

HTMLite is meant to be loosely compatible with WHATWG's HTML specification whilst being tractible to understand and implement.
Reflecting what's supported/used by most browser engines and web pages, rather than the popular few.

HTMLite is an application of [XMLite](https://codeberg.org/Weblite/XMLite/src/branch/draft/XMLite.md), and is based fundamentally on XMLite-Model.
It also defines the HTML syntax as an alternative to XMLite-Syntax.

## Status of This Document
This specification is written, with external feedback, by an amateur browser engine developer dissatisfied by WHATWG, in the style of a W3C specification.
It is not yet ratified by any other relevant developers.

## Chapter 0: Introduction
### Reading This Specification
This section is non-normative.

This specification has been written with two types of readers in mind: HTML authors & HTML implementors.
We hope the specification will provide authors with the tools they need to write accessible, informative documents that can be read in any web browser on any digital device, without exposing them to implementation details.
Implementors, however, should find all they need to build to correctly render a majority of webpages.

This specification is split into chapters which may be read in any order.
The appendices provide additional details which may aid implementation but are non-normative.
Any section described as non-normative is not required for successful implementation.

### How The Specification is Organized
This section is non-normative.

Chapter 1 (following these introductions) provides everything you need to know to successfully parse a majority of documents.
Appendix A proposes one straightforward approach to parsing invalid documents, though not all user agents follow it.

Chapter 2 describes the intended meaning and behaviour of all known HTML attributes, apart from those only used within webforms.

Chapter 3 describes the intended meaning of all known HTML elements, apart from those only used within webforms.

Chapter 4 describes the additional HTML attributes & elements used to solicit information from the reader.

Appendix B proposes a user agent CSS stylesheet for visual rendering of HTML.
This same CSS may be found in the CSS2 specification, which the CSS3 module specifications extend.

Appendix C suggests an order in which user agents may wish to implement elements in, and provides a vocabulary for user agents to describe which elements they do support.

Appendix D lists related specifications referenced by this one.

### Conventions
* Element names are denoted with surrounding triangular brackets (`<` & `>`), regardless of any attributes the author might add.
* Attribute names are denoted with a preceding `@`-sign.
* Any other inline syntax example will be highlighted `like this`.
* All parsing rules will be described both in prose & [EBNF](http://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf) notation. Where the two (accidentally) conflicts, the EBNF should take precedance.
* In case of EBNF ambiguity, greedy parsing semantics should be interpreted. Sequences should be terminated as soon as possible.

### Design Principles
This section is non-normative.

* **Simplicity**. HTML is a simple language that is human readable and writable. It should be straightforward for user agents to parse whilst remaining easy for humans to understand in its source code form.
* **Vendor, platform, and device independence**. The intended structural meaning of any HTML document should remain perceivable regardless of whether its rendered the way its author intends.
* **Forward and backward compatibility**. HTMLite0 user agents will be able to roughly understand any HTML1 document, and any HTML1 user agent will be able to understand any HTMLite0 document. HTMLite0 user agents may parse to a different structure when presented with loosely-structured documents permitted by previous versions of HTML, but they should still understand similar semantics.
* **Maintainability**. By pointing to style sheets from documents, webmasters can simplify site maintenance and retain consistent look and feel throughout the site. For example, if the organization's background color changes, only one file needs to be changed.
* **Flexibility**. Space should be left for authors to extend HTML semantics where there's gaps for their usecase.
* **Richness**. HTMLite should provide a variety of elements with which authors can express themselves.
* **Accessibility**. HTML describes the semantic structure of documents seperate from how those semantics are communicated, so they can be communicated differently depending on the reader's needs.

### Definitions
The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [BCP 14](https://www.rfc-editor.org/info/bcp14).
However, for readability, these words do not appear in all uppercase letters in this specification.

At times, this specification recommends good practice for authors and user agents.
These recommendations are not normative and conformance with this specification does not depend on their realization.
These recommendations contain the expression "We recommend ...", "This specification recommends ...", or some similar wording.

The fact a feature is marked "deprecated" or "discouraged" also no influence on confirmance.
The word "discouraged" is used to indicate a feature is likely to cause present or future breakage in covered browsers, without remarking on the stance of W3C or WHATWG.

All sections of this specfication, excluding appendices, are normative unless marked otherwise.

<dl>
  <dt>Element</dt>
  <dd>An XMLite-Model element, the basic unit of HTML markup as defined by HTMLite and the primary syntactic construct of the HTML syntax.</dd>
  <dt>Attribute</dt>
  <dd>A value associated with an element, consisting of a name, and an associated (textual) value.</dd>
  <dt>Content</dt>
  <dd>The content associated with an element in the source document. Some elements have no content, in which case they are called <strong>empty</strong>. The content of an element may include text, and it may include a number of sub-elements, in which case the element is called the <strong>parent</strong> of those sub-elements.</dd>
  <dt>Document tree</dt>
  <dd>The hierachy of elements making up an XMLite-Model document. Each element in this tree has exactly one parent, with the exception of the <strong>root</strong> element, which has none.</dd>
  <dt>Child</dt>
  <dd>An element A is called the child of element B if and only if B is the parent of A.</dd>
  <dt>Descendant</dt>
  <dd>An element A is called a descendant of an element B, if either (1) A is a child of B, or (2) A is the child of some element C that is a descendant of B.</dd>
  <dt>Ancestor</dt>
  <dd>An element A is called an ancestor of an element B, if and only if B is a descendant of A.</dd>
  <dt>Author</dt>
  <dd>An author is a person who writes documents and associated style sheets. An <strong>authoring tool</strong> is a User Agent that generates documents.</dd>
  <dt>Reader</dt>
  <dt>User</dt>
  <dd>A user is a person who interacts with a user agent to view, hear, or otherwise use a document and any related resources.</dd>
  <dt>User agent (UA)</dt>
  <dd>A user agent is any program which processes or renders a HTMLite document in any form according to the terms of this specification. Whether or not it incorporates related resources in that rendering.</dd>
  <dt>Open Tag Stack</dt>
  <dd>A last-in-first-out collection of elements used in the process of parsing the HTML syntax into an XMLite-Model document.</dd>
  <dt>Current element</dt>
  <dd>The most recently added element currently in the <strong>open tag stack</strong>, to which the parser is appending elements.</dd>
</dl>

### Acknowledgements
Several sections of this spec are derived from Mozilla Developer Network under CC-By-SA.

## Chapter 1: Parsing
Documents are received by user agents as a sequence of bytes (hereafter referred to as its "source code") which need to be parsed into into a document tree.
This chapters describes how user agents should do so.

All parsing rules will be phrased both in prose and EBNF, where the EBNF emits a sequence of tokens for the parser to restructure into a document tree via the open tag stack.

        start := token*

### Content Types
Documents are transmitted over the internet or fetched from the local filesystem where other content types may, and likely are, present.
The structure of the transmission, termed a message entity, is defined by [RFC 2045](http://www.ietf.org/rfc/rfc2045.txt) and [RFC 2616](http://www.ietf.org/rfc/rfc2616.txt).

#### The `text/html` Content Type
A message entity with a content type of "`text/html`" indicates that the entity is an independent HTML document in the HTML syntax.
How these are parsed is specified by the rest of this chapter.
All elements and attributes in a `text/html` document implicitly have the namespace "`http://www.w3.org/1999/xhtml`".

The `text/html` content type was first registered in IETF [RFC1866](https://datatracker.ietf.org/doc/html/rfc1866) by T. Berners-Lee and D. Connolly.

#### The `application/xhtml+xml` Content Type
A message entity with a content type of "`application/xhtml+xml`" indicates that the entity is an independent HTML document in the XML syntax, known as XHTML.
User agents should parse these according to [XMLite-Syntax](https://codeberg.org/Weblite/XMLite/src/branch/draft/XMLite.md), and must not parse them according to this chapter.
The empty namespace prefix is pre-defined in an `application/xhtml+xml` document as referring to `http://www.w3.org/1999/xhtml`, but authors should declare this explicitly in the `xmlns` attribute of the root element.

User agents should treat elements and attributes in any XML document, regardless of content type, that have the namespace `http://www.w3.org/1999/xhtml` as HTML elements or attributes.
They may also interpret elements or attributes with no namespace, or attributes that are inheriting their element's namespace, which match the local names of HTML elements or attributes as those HTML elements or attributes.
User agents should allow elements in non-HTML content types to exist with ancestors other than those otherwise required by this specification; in particular, the root element should not be required to be `<html>` in other content types.

The `application/xhtml+xml` content type was first registered in W3C Superceded Recommendation [XHTML™ 1.0 The Extensible HyperText Markup Language](https://www.w3.org/TR/xhtml1/).

#### The `text/x-kdl` Content Type
A message entity with a content type of "text/x-kdl" represents an independent KDL encoding of XHTML.
User agents may parse these according to the [KDL](https://github.com/kdl-org/kdl/blob/main/SPEC.md) and [XML in KDL](https://github.com/kdl-org/kdl/blob/main/XML-IN-KDL.md) specifications, producing an XMLite-Model document handled as with `application/xhtml+xml`.

**Disclaimer** No known user agents currently support KDL, this subsection is more of a proposal. While KDL is solidly standardized, no content type has yet been registered for it. `text/x-kdl` is a placeholder.

### Text Encodings
The received text is interpreted as Unicode characters according to a specified text encoding.
Authors must place a `@charset` attribute of value "utf-8" in the document, which should be on the first `<meta>` tag.
Authors may use another value from the [IANA Character Sets Registry](http://www.iana.org/assignments/character-sets/character-sets.xhtml) if the text is encoded differently.
User agents must support the "utf-8" character set which should be the default, though they may support other character sets and/or default to Latin1.

**NOTE** Both Latin1 and UTF-8 are supersets of ASCII, so the above rules guarantee that ASCII documents would be understood without use of `@charset`.

### Parsing Tags
A tag in HTML is a syntactic construct starting with `<` and ending with `>`.
The semantics of the tag is determined by the character(s) immediately following that starting `<`.

        token := "<" tag .

#### Comments
If the first characters of a tag are `<!--` then this represents a "comment" which user agents must ignore whilst rendering, or should preserve in transformations where possible.
A comment tag is terminated by `-->`, before which can be any character including but not limited to `>`.
Comments are intended to allow authors to add notes to themselves and collaborators in the HTML source code describing the intent of markup.

        tag := "!--" any* "-->" .

#### Special Tags
If the first characters of a tag are `<!` and it is not a comment, it is referred to as being "special".
These tags are not part of the document tree, but may influence how it is handled.
Special tags contain a name followed by additional text (not including a `>`) parsed and handled according to that name, if supported.

        tag := "!" ident any* ">" .

Authors should prefix their HTMLite documents with `<!DOCTYPE html>` to disable "quirks mode" in browsers which have one for the sake of backwards compatibility.
This is an example of a special tag.

#### Opening Tags
If there's no additional punctuation before the tag's name, it is considered an "opening tag".
Opening tags may include attributes after their name as described in a later section.
Authors may add a slash (`/`) character before the closing triangular bracket (`>`) to indicate this element is self-closing.
Authors should only add this trialling slash if the element is, in fact, defined below as self-closing.

        tag := ident attr* "/"? ">" .

Upon encountering an opening tag, a user agent should:

1. Create a new XMLite-Model element with the given name, under the namespace `http://www.w3.org/1999/xhtml`, including all attributes, under the same namespace. The namespace must not be specified explicitly in the source code.
2. Append it as a child of the current top of open tag stack.
3. Push it onto the open tag stack making it the current element, unless this specification says otherwise.

##### Self-closing Tags
If the tagname is one of the following, it is "self-closing" and contains no children.
That is step 3 should be ignored for these tagnames.

* `<area>`
* `<base>`
* `<br>`
* `<col>`
* `<command>`
* `<embed>`
* `<hr>`
* `<img>`
* `<input>`
* `<keygen>`
* `<link>`
* `<meta>`
* `<param>`
* `<source>`
* `<track>`
* `<wbr>`

The semantics of these tags are described in later chapters.
The intention is not to extend this list.

User agents may self-close any tags with a trailling slash character, though this is not required.

##### Special handling for `<script>` Tags
Upon encountering a `<script>` tag, all subsequent text except the corresponding closing `</script>` tag must be parsed to text nodes regardless of any other syntactic grammar rules it might match.
That closing tag is treated normally as a closing tag.

While the `<script>` tag is defined in Chapter 3, most browsers ignore its inner text.

#### Closing Tags
If the initial characters of a tag are `</` then it is a "closing tag" and must only contain the name of the current element.
If it contains any other name, that is undefined behaviour user agents may handle however they want.
User agents shouldn't error out upon encountering such "unbalanced tags" but equally authors shouldn't write their documents to contain unbalanced tags.
Appendix A proposes non-normative behaviour for handling this case.

        tag := "/" ident ">" .

Upon encountering a valid closing tag that element should be popped off the open tag stack, leaving the subsequent tag to be appended to its parent's children.

Tags not being closed before the end of the document must not be treated as a syntax error.

### Attributes
An attribute allows authors to attach additional information to an element beyond the text they want user agents to render such that's perceivable to the reader, each consisting of a name and optional value.
Omitting the value is equivalent to setting the value to an empty value (i.e. `checked=""` is equivalent to `checked`).
Attributes are specified in the element's open tag seperated from each other and the tagname by whitespace.

An attribute syntactically starts with its name (implicitly under the namespace `http://www.w3.org/1999/xhtml`, which must not be specified explicitly), optionally followed by an equals sign (`=`) and its value.
The value may optionally be surrounded by either single or double quotes.
If it is not surrounded by any quotes the value ends either when the tag ends or at the next sequence of whitespace.

        attr := ws* ident ["=" value] .
        value := "'" text "'" | '"' text '"' | text .

All valid attributes are defined in chapter 2.

### Names
Attribute or tag names must consist of a sequence of letters (as defined by Unicode) and dashes (`-`).
User agents may allow other characters but authors must not rely on any such behaviour.

        ident := (letter | "-")* .

TBD: Namespaces? How widely supported 

### Text
Any text occuring between elements must be appended as a child of the current element.
Upon failing to parse a tag a user agent must parse the `<` as such a text child.

        text := any text .
        text := .
        tag := .
        token := text

This represents the text which will be rendered, with the tags added formatting.

### Entities
User agents should replace any text between `&` and `;` inclusive, occurring within attribute values or between tags, with the character identified by that intermediary text.

        text := "&" entity ";" .

#### Numeric Entities
If the entity starts with a `#` then it identifies the character which should replace it by it is a number.
If the subsequent is `x` then the number is written in base 16, otherwise it is written in base 10.
Upon failing to parse an entity user agents must parse the `&` as a text child representing said "&".

        entity := "#x" ('0'-'9' | 'a'-'f' | 'A'-'F')+ .
        entity := "#" ('0'-'9')+ .

#### Named Entities
Entities may identify certain Unicode characters by name, as defined by [W3C's reference](https://dev.w3.org/html5/html-author/charref).
The most important of these are:

* `&lt;` should be replaced by "<".
* `&gt;` should be replaced by ">".
* `&amp;` should be replaced by "&".
* `&quot;` should be replaced by an ASCII double quote ('"').
* `&apos;` should be replaced by an ASCII apostrophe/single quote ("'").

        entity := ident .

### Character Classes
In the EBNF, `any` refers to any single Unicode character.
`letter` specifically refers to any character classified as a letter by Unicode.
`ws` refers to any character classified as whitespace by Unicode.

TBD: Refer to Unicode spec for equivalent EBNF rules.

### EBNF
All the EBNF notation above is repeated in a single chunk below, where parsing should start from the `start` rule:

        start := token* .
        token := text | "<" tag .
        text := any text | "&" entity ";" | .
        entity := "#x" ('0'-'9' | 'a'-'f' | 'A'-'F')+ | "#" ('0'-'9')+ | ident .
        ident := (letter | "-")* .

        tag := "!--" any* "-->" | "!" ident any* ">" | ident attr* ">" | "/" ident ">" .
        attr := ws* ident ["=" value] .
        value := "'" text "'" | '"' text '"' | text .

## Chapter 2: Attributes
XMLite-Model attributes attach additional info to an element which isn't part of the text normally rendered.
User agents may support whichever of these attributes they want, though they should support `@href` and `@src`.
The term "supporting user agents" will be used to exclude user agents which don't support the attribute being discussed.
Most attributes may be applied to any element, though some attributes may have certain elements supporting user agents must support them on.

All attributes defined by HTMLite are in the namespace `http://www.w3.org/1999/xhtml`.

User agents must either ignore unsupported attributes or let user and/or author provided stylesheets handle them.
The same holds for all attributes on unsupported elements.
Text stating supporting user agents must support the attribute on specific elements does not require said user agent to support those elements.

Chapter 4 discusses attributes and elements specific to solicitating additional information from readers.

### @accesskey Attribute
The accesskey attribute provides a hint for generating a keyboard shortcut for the current element.
The attribute value must consist of a single printable character (which includes accented and other characters that can be generated by the keyboard).

Supporting user agents may allow multiple space seperated characters to be specified, in which case the first one supported by the attached keyboard will be selected.
However authors should not use this feature until support is more common.

### @alt Attribute
Provides human readable text for the user agent render in place of the linked resource to be embedded in case it can't, or fails to, load the linked resource to be embedded.

### @charset Attribute
Supporting user agents must support this on `<meta>` elements.

Specifies the name, from [IANA Character Set Registry](http://www.iana.org/assignments/character-sets/character-sets.xhtml), of the text encoding in which this document is written in.

### @cite Attribute
Specifies a URL to a document which explains the contents of the element it is attached to.

### @class Attribute
Specifies whitespace-seperated author-defined words classifying this element.
These words may be used in stylesheets or scripts applied to this document, but must not otherwise hold meaning to the user agent.

### @colspan Attribute
Only valid on `<td>` or `<th>` elements.

Specifies a base 10 number of columns accross which the table cell (see Chapter 3's definition of `<table>`) should span.

### @content Attribute
Only valid on `<meta>` elements.
See `<meta>`'s definition for details on this attribute.

### @contextmenu Attribute
**NOTE** Deprecated by WHATWG, not by HTMLite.

Specifies which `<menu>` element should be used as, possibly a subset of, the context menu (whatever "context menu" means to the user agent) for the attached element.
The value of this attribute should match the value of the `@id` on the referenced `<menu>`.

### @controls Attribute
Supporting user agents must support this attribute on `<video>` and `<audio>` elements.

The presence of this attribute, regardless of its value, insists that playback controls must be attached to the linked timed resource to be embedded, regardless of whether that is the default behaviour.

### @coords Attribute
Only valid on `<area>` elements. See its definition for full details.

Specifies a comma (`,`) seperated list of numbers specifying the position of the area link in pixels.

### @crossorigin Attribute
TBD

### @datetime Attribute
Specifies the timestamp in [ISO8601 format](https://www.iso.org/iso-8601-date-and-time-format.html) which the element (e.g. `<time>`) represents, or the timestamp when the operation it represents (e.g. `<ins>` or `<del>`) happened.

### @decoding Attribute
Supporting user agents must support this attribute on `<img>`.

Specifies whether to what for the image to be download before displaying the page.
This attribute must have a value of "sync" indicating the page should only be displayed after the image has been downloaded, "async" to indicate the page may display before the image has been downloaded, or "auto" to leave the choice up to the user agent.

### @default Attribute
Only valid on `<track>`.

Specifies that this track should be enabled until the reader's preferences specifies otherwise.

### @dir Attribute
Specifies the directionality of contained text. This attribute must have a value of "ltr" for left-to-right rendering, "rtl" for right-to-left rendering, or "auto" to let the user agent decide.

### @download Attribute
Indicates the linked resource should be saved to the reader's local filesystem.

### @headers Attribute
Only valid on `<td>` or `<th>` elements.

References the `<th>` elements for this table cell by the whitespace-seperated values of their `@id` attributes.

### @height Attribute
Supporting user agents must support this attribute on `<canvas>`, `<embed>`, `<iframe>`, `<img>`, `<input>`, `<object>`, and `<video>` elements.

Specifies as a base 10 number how tall the linked resource to be embedded is in pixels.

### @high Attribute
Specifies the maximum value of a `<meter>` element as a base 10 number.

### @href Attribute
Supporting user agents should must support this on `<a>`, `<area>`, `<base>`, & `<link>`.
Some user agents may support on any elements, but authors should not rely on that behaviour.

Specifies a URL the user can navigate to by selecting, by some means, this element.
The resource at that URL will be referred to by other attributes on the same element as the "linked resource".

### @hreflang Attribute
Specifies the language, by its [BCP 47](https://www.rfc-editor.org/info/bcp47) language tags, of the linked resource.

### @http-equiv Attribute
Only valid on `<meta>` elements. See `<meta>`'s definition for details on this attribute.

### @id Attribute
Specifies one or more author defined which each uniquely defines this element within the document.
Other attributes, or the document's URIs' anchor component, may reference elements by referencing one of these words.
These words may be used in stylesheets or scripts applied to this document.

User agents must not otherwise attach special meaning to specific values of `@id`.

### @importance Attribute
Specifies the relative fetch priority for the resource.

TBD: Is this ascending or descending? What's the default?

### @integrity Attribute
The hash of the linked resource for use in verifying it was successfully downloaded.

TBD: Specify the format of its value.

### @ismap Attribute
Authors are discouraged from using this attribute for the sake of user agents which don't use visual output or a mouse input.

Specifies the linked resource to be embedded is a serverside image map.
Telling supporting user agents that they should send the coordinates of where the user clicked on the image to the server when loading a linked resource for an ancestor element.

TBD: How should those coordinates be sent?

### @itemprop Attribute
Used to aid user agents in "scraping" machine-readable data from a webpage.
Indicates a new property (consisting of the attribute's value and the attached element's inner text) should be added to the "item" corresponding to the nearest ancestor with an "itemscope" attribute.

### @itemscope Attribute
Used to aid user agents in "scraping" machine-readable data from a webpage.
When present indicates supporting user agents must allocate a new "item" corresponding to this element.

### @itemtype Attribute
Used to aid user agents in "scraping" machine-readable data from a webpage.
Specifies a URI or other text classifying the item corresponding to this element.

### @kind Attribute
Only valid on text `<track>` elements.

Specifies how the text track should be used. The value should be one of:

* **subtitles** indicates it links a translation to another natural language of any spoken audio, and may include translations of text shown in the video.
* **captions** indicates it links a transcription, or possibly natural language translation, of the audio including any sound effects. Suitable for deaf viewers or use when audio is muted.
* **descriptions** indicates it links a textual description of the video contents.
* **chapters** indicates it links labels to aid navigating between different temporal segments of the video.
* **metadata** indicates the linked resource should be ignored, except by any scripts applied to the page.

If it is another value user agents must treat the linked text resource as "metadata".
If this attribute is absent user agents must treat the linked resource as "subtitles".

### @lang Attribute
Specifies the natural language used in this element by [BCP 47](https://www.rfc-editor.org/info/bcp47) language tag.

### @loading Attribute
Supporting user agents must support this attribute on `<img>` and `<iframe>` elements.

If the value is `lazy` supporting user agents should not load the linked resource to be embedded until it would be visible.
If the value is `eager`, or this attribute is absent, user agents should load the linked resource immediately.

**NOTE** Since this attribute can leak how far through the page the reader has scrolled to server, user agents might not want to implement this attribute.

### @loop Attribute
Supporting user agents must support this attribute on `<audio>`, `<bgsound>`, `<marquee>`, and `<video>` elements.

Indicates the linked finite time-based resource to be embedded should repeat from the begining once it finishes playing.

### @low Attribute
This attribute is only valid on `<meter>` elements.

Specifies the minimum value for a `<meter>` element as a base 10 number.

### @media Attribute
Supporting user agents must support this attribute on `<a>`, `<area>`, `<link>`, `<source>`, and `<style>` elements.

Specifies a CSS media query indicating which devices the linked resource was designed for.

### @muted Attribute
Supporting user agents must support this attribute on `<audio>` and `<video>` elements.

If present indicates the auditory component of the linked resource to be embedded should be initially set to be muted until otherwise specified by the reader.

### @name Attribute
This attribute's meaning depends on which type of the element it is attached to.
As such this attribute's meaning is defined in Chapters 3 and 4 alongside the relevant elements.

### @open Attribute
This attribute is only valid on `<details>` and `<dialog>` elements.

When present indicates the full contents of the attached element should be visible, until the reader or an attached script says otherwise.

### @optimum Attribute
This attribute is only valid on `<meter>` elements.

Specifies the optimal value for the `<meter>` element as a base 10 number.

### @ping Attribute
Specifies a space-seperated list of URLs to send POST requests to with the body "PING" upon navigating to the linked resource.

**NOTE** Many user agents oppose such a feature, whilst others wish to discourage authors from implementing the same feature in a less performant way.

### @poster Attribute
Supporting user agents must support this attribute on `<video>` elements.

Specifies a URL to an image to represent the video before loading the linked video to be embedded itself.

### @preload Attribute
Supporting user agents must support this attribute on `<video>` elements.

Specifies how much of the linked video to be embedded should be downloaded upon page load.
If the value is `auto` supporting user agents should download the full video.
If the value is `metadata` the user agent should download only enough of the video to read its embedded metadata.
If the value is `none` the user agent should not download the full video upon page load.
If the attribute is missing user agents may however fetch as much of the video as it wants.

### @referrerpolicy Attribute
Specifies how much of the current URL, if any, to include in the HTTP Referer header when fetching the linked resource.

TBD: list valid values and their meanings.

### @rel Attribute
Specifies how the linked resource relates to this document as whitespace-seperated words.
Valid values, of which this attribute may hold multiple due to that whitespace-separation, are listed by [IANA](https://www.iana.org/assignments/link-relations/link-relations.xhtml), [WHATWG](https://html.spec.whatwg.org/multipage/links.html#linkTypes), and the [microformats wiki](https://microformats.org/wiki/existing-rel-values).

### @reversed Attribute
This attribute is only valid on `<ol>` elements.

If present indicates that the list should be rendered in a descending order rather than ascending.

### @rowspan Attribute
This attribute is only valid on `<th>` and `<td>` elements.

Specifies a base 10 number of rows the table cell (see Chapter 3's definition of `<table>`) should cover.

### @sandbox Attribute
Supporting user agents must support this attribute on `<iframe>` elements.

Specifies whitespace seperated words identifying which permissions should be enabled for the linked document to be embedded.
If the value is empty, supporting user agents must disable all permissions.
If the attribute is absent supporting user agents must enable all permissions.

While additional permissions may be added by other specifications, valid permissions include:

* `allow-downloads-without-user-activation` allows for downloads to occur without user gestures in the linked document.
* `allow-downloads` allows for downloads to occur after the user has interacted with the linked document.
* `allow-forms` allows the linked document to submit forms.
* `allow-popups` allows links within the linked document to open in target `_blank`, or scripting equivalents.
* If `allow-popups-to-escape-sandbox` isn't specified any popups (as per `allow-popups`) no longer inherits this sandbox.
* If `allow-same-origin` isn't specified the "same origin test" for any storage no longer longer always fails.
* `allow-top-navigation` and `allow-top-navigation-by-user-activation`  allows links within the linked document to open in target `_top`. `allow-top-navigation` also allows for scripts to load a new document there.

### @scope Attribute
This attribute is only valid on `<th>` elements.

Specifies which other table cells this table header applies to.
Its value may be one of `row`, `col`, `rowgroup`, `colgroup` specifying the containing table component to who's table cells it relates to.
These terms are defined in Chapter 3's definition of `<table>`.

### @shape Attribute
This attribute is only valid on `<area>` elements.

Specifies whether the geometric shape of the region of the areamap covered by this `<area>` element.
Its value may be one of `rect`, `circle`, `poly`, or `default` where `default` indicates clicking any other of the imagemap's `<area>`s should activate this `<area>`.

Authors are discouraged from user this attribute to better cater to non-visual browsers.

### @sizes Attribute
TBD

### @src Attribute
Supporting user agents must support this attribute on `<audio>`, `<embed>`, `<iframe>`, `<img>`, `<input>`, `<script>`, `<source>`, `<track>`, `<video>` elements.

Specifies the URL of a related resource which user agents should embed in this document and render in this element's place.
Failing that it may be treated by user agents the same as `@href`.
The resource URL refers to will be called "the linked resource to be embedded".

### @srcdoc Attribute
Supporting user agents must support this attribute on `<iframe>` elements.

Specifies literal HTML to load into this element as if from a `data:` URI in a `@src` attribute.

### @srclang Attribute
Supporting user agents must support this attribute on `<track>` elements.

Identifies the natural language of the linked resource by ISO language code.

### @srcset Attribute
TBD

### @start Attribute
Only valid on `<ol>` elements.

Defines the base 10 number of the first item in the list, from which subsequent numbers increment.

### @style Attribute
Specifies CSS style properties in CSS syntax, using the [CSS declarations syntax](https://www.w3.org/TR/CSS22/syndata.html#declaration).

### @tabindex Attribute
Indicates the element can be focused (may be selectively or entirely ignored if scripts are disabled or unsupported) and in which order the tab key (or equivalent) should navigate between these items.
Its value should be a signed base 10 number.

Positive numbers occur first in tab order in ascending order, followed by elements tabindex 0 or focusable elements without this attribute.
Supporting user agents may prevent readers from interacting with elements with negative tabindex outside of applied scripts.

**NOTE** Authors should avoid the need to use this property, as it indicates you likely have accessibility issues.

### @target Attribute
Indicates which window, tab, or `<iframe>` (collectively referred to as "browsing context) the linked resource should open in.
The value of this attribute may be `_self` to indicate it should open in the same browsing context as the current document, `_blank` opens a new tab or window to open the linked resource in, `_parent` to open in the browsing context containing `_self`, or `_top` to indicate it should be opened in the same tab this document is currently open in ignoring any intermediary `<iframe>`s.
It may also also share the same value as the `@name` attribute of any `<iframe>` in the same tab or window to indicate it should open there.
If that iframes not found a new tab or window should be opened for any future link with the same `@target` value to open in.

### @title Attribute
Provides explanatory text to be shown when the reader requests it from the element in some way.
For example on a desktop browser this may be shown, after some delay, as a "tooltip" when the user hovers over the attached element.

### @translate Attribute
Indicates whether or not a machine translation tool should localize the text in this element, where children elements don't have their own `@translate` attributes.
A value of `yes` or an empty string indicates it should. A value of `no` indicates it shouldn't.

### @type Attribute
Indicates the content type, from [IANA's media type registry](https://www.iana.org/assignments/media-types/media-types.xhtml), of the linked resource.

### @usemap Attribute
Supporting user agents must support this attribute on `<img>`, `<input>`, and `<object>` elements.

Indicates the `@id`, optionally prefixed with `#`, of the `<map>` element to be overlaid on this image.

### @width Attribute
Supporting user agents must support this attribute on `<canvas>`, `<embed>`, `<iframe>`, `<img>`, `<input>`, `<object>`, and `<video>` elements.

Specifies as a base 10 number how tall the linked resource to be embedded is in pixels.

## Chapter 3: Elements
The name of an element defines the semantic meaning of its structure, and in turn how the document should be rendered.
The details of how they render an element is left up to the user agent, though appendix B suggests how this can be done visually in machine readable CSS.
Certain element names instead alter how the rest of the page is rendered.

All elements defined by HTMLite are in the namespace `http://www.w3.org/1999/xhtml`.

User agents are not required to support any particular element.
"Supporting user agents" will refer to any user agent which supports the element being discussed.
Unless otherwise specified, elements should be a descendant of the `<body>` element.
"`@src` and related attributes" refers to any attribute whose definition refers to the "linked resource to be embedded", though user agents may support however many of them they wish despite suggestions to the contrary.
Similar for "`@href` and related properties".

Elements a user agent doesn't support, including names not listed here, must render their elements as if they weren't wrapped in the unsupported element.
If there's any scripts or stylesheets applied to the page the unsupported elements must be exposed to them.

TBD: How to incorporate MathML and SVG?

### `<a>` Elements
`<a>` HTML elements (or "anchor" elements), with its href attribute, creates a hyperlink to web pages, files, email addresses, locations in the same page, or anything else a URL can address.

### `<abbr>` Elements
`<abbr>` HTML elements represents an abbreviation or acronym; the optional `@title` attribute can provide an expansion or description for the abbreviation.
If present, `@title` must contain this full description and nothing else.

### `<address>` Elements
<address> HTML elements indicates that the enclosed HTML provides contact information for a person or people, or for an organization.

### `<article>` Elements
`<article>` HTML element represents a self-contained composition in a document, page, application, or site, which is intended to be independently distributable or reusable (e.g., in syndication).
Examples include: a forum post, a magazine or newspaper article, or a blog entry, a product card, a user-submitted comment, an interactive widget or gadget, or any other independent item of content.

### `<aside>` Elements
`<aside>` HTML elements represents a portion of a document whose content is only indirectly related to the document's main content.
Asides are frequently presented as sidebars or call-out boxes.

### `<audio>` Elements
`<audio>` HTML element is used to embed sound content in documents.
It may contain one or more audio sources, represented using the `@src` attribute or the `<source>` element: the browser will choose the most suitable one.

Supporting user agents must support `@src` and related attributes on `<audio>` elements, and may support `<source>` elements, to specify which audio resource it plays.
Supporting user agents may support `@controls`, `@loop`, `@muted`, & `@preload`, and possibly `@autoplay` attributes.

### `<b>` Elements
`<b>` HTML elements is used to draw the reader's attention to the element's contents, which are not otherwise granted special importance.

### The `<base>` Element
The `<base>` HTML element specifies the base URL to use for all relative URLs in a document.
There can be only one `<base>` element in a document.

Authors must include `@href` on any `<base>` element, specifying the URL all other relative URLs on the page must be interpreted as being relative to.

### `<bdi>` Elements
`<bdi>` HTML elements tells user agent's bidirectional algorithm (if present) to treat the text it contains in isolation from its surrounding text.
It is particularly useful when a website dynamically inserts some text and doesn't know the directionality of the text being inserted.

### `<bdo>` Elements
`<bdo>` HTML elements overrides the current directionality of text, so that the text within is rendered in a different direction.

### `<blockquote>` Elements
`<blockquote>` HTML elements indicates that the enclosed text is an extended quotation.
A URL for the source of the quotation may be given using the `@cite` attribute, while a text representation of the source can be given using the `<cite>` element.

### The `<body>` Element
The `<body>` HTML element represents the content of an HTML document.
There can be only one <body> element in a document.
It must be a direct child of `<html>`, and should be the last child.

### `<br>` Elements
`<br>` HTML elements produces a line break in text (carriage-return).
It is useful for writing a poem or an address, where the division of lines is significant.

### `<cite>` Elements
`<cite>` HTML elements is used to describe a reference to a cited creative work, and must include the title of that work.
The reference may be in an abbreviated form according to context-appropriate conventions related to citation metadata.

### `<code>` Elements
`<code>` HTML elements displays its contents styled in a fashion intended to indicate that the text is a short fragment of computer code.

### `<data>` Elements
`<data>` HTML elements links a given piece of content with a machine-readable translation.
If the content is time- or date-related, the time element must be used.

### `<del>` Elements
`<del>` HTML elements represents a range of text that has been deleted from a document.
This can be used when rendering "track changes" or source code diff information, for example.
`<ins>` elements can be used for the opposite purpose: to indicate text that has been added to the document.

Supporting user agents may support `@cite` and `@datetime` on `<del>` elements.

### `<details>` Elements
`<details>` HTML elements creates a disclosure widget in which information is visible only when the widget is toggled into an "open" state. A summary or label must be provided using the summary element. `<details>` elements should contain a `<summary>` element as one of its children.

Supporting user agents may support `@open` on `<details>` elements to configure whether it defaults to the "open" state upon page load.

#### `<summary>` Elements
`<summary>` HTML element specifies a summary, caption, or legend for a details element's disclosure box.
Clicking the `<summary>` element toggles the state of the parent `<details>` element open and closed.
`<summary>` elements must be a direct child of `<details>` elements.

### `<dfn>` Elements
`<dfn>` HTML element is used to indicate the term being defined within the context of a definition phrase or sentence.
The `<p>` element, the `<dt>`/`<dd>` pairing, or the `<section>` element which is the nearest ancestor of the `<dfn>` is considered to be the definition of the term.

### `<div>` Elements
`<div>` HTML elements are the generic container for flow content.
It has no effect on rendering unless a stylesheet or script is applied.

Essentially `<div>` is a reserved tagname for authors to use however they wish, though authors should avoid using it because without special care it'll prevent your page from being device independent.

### `<dl>` Elements
`<dl>` HTML elements represents a description list.
The element encloses a list of groups of terms (specified using the `<dt>` element) and descriptions (provided by `<dd>` elements).
Common uses for this element are to implement a glossary or to display metadata (a list of key-value pairs).

#### `<dd>` Elements
`<dd>` HTML elements provides the description, definition, or value for the preceding term (`<dt>`) in a description list (`<dl>`).
It must be a direct child of a `<dl>` element.

#### `<dt>` Elements
`<dt>` HTML elements specifies a term in a description or definition list, and as such must be used inside a `<dl>` element.
It is usually followed by a dd element; however, multiple `<dt>` elements in a row indicate several terms that are all defined by the immediate next dd element.

### `<em>` Elements
`<em>` HTML elements marks text that has stress emphasis.
`<em>` elements can be nested, with each level of nesting indicating a greater degree of emphasis.

### `<figure>` Elements
`<figure>` HTML elements represents self-contained content, potentially with an optional caption, which is specified using the figcaption element.
The figure, its caption, and its contents are referenced as a single unit.

#### `<figcaption>` Elements
`<figcaption>` HTML elements represents a caption or legend describing the rest of the contents of its parent figure element.
It must be a direct child of a `<figure>` element.

### `<footer>` Elements
`<footer>` HTML elements represents a footer for its nearest sectioning content or sectioning root element.
A <footer> typically contains information about the author of the section, copyright data or links to related documents.

### The `<head>` Element
The `<head>` HTML element contains machine-readable information (metadata) about the document, like its title, scripts, and style sheets.
It must be a direct child of `<html>`, and should be the first child.

### `<header>` Elements
`<header>` HTML elements represents introductory content, typically a group of introductory or navigational aids.
It may contain some heading elements but also a logo, a search form, an author name, and other elements.

### `<hr>` Elements
`<hr>` HTML elements represents a thematic break between paragraph-level elements: for example, a change of scene in a story, or a shift of topic within a section.

### `<h1>`, `<h2>`, `<h3>`, `<h4>`, `<h5>`, and `<h6>` Elements
`<h1>` to `<h6>` HTML elements represent six levels of section headings.
`<h1>` (of which there should only be one, outside of any `<section>` where this rule applies recursively) is the highest section level and `<h6>` is the lowest.

Authors should use `<section>` in place of `<h2>` to `<h6>`, though user agents must still support `<h2>` to `<h6>`.

### The `<html>` Element
The `<html>` HTML elements represents the root (top-level element) of an HTML document, so it is also referred to as the root element.
All other elements must be descendants of this element.

### `<i>` Elements
`<i>` HTML elements represents a range of text that is set off from the normal text for some reason, such as idiomatic text, technical terms, taxonomical designations, among others.

### `<iframe>` Elements
`<iframe>` HTML elements represents a nested browsing context, embedding another HTML page into the current one.
They must have a `@src` and related attributes.

Supporting user agents must support `@src` and related attributes on `<iframe>`, and may support `@name` (to be referenced via `@target` attributes), `@sandbox`, and `@srcdoc` attributes.

### `<img>` Elements
`<img>` HTML element embeds an image into the document.
`<img>` elements must have a `@src` attribute and should have a `@alt` attribute.

Supporting user agents must support `@src` and related attributes on `<img>` elements.
They should support `@alt`, and may support `@sizes` and `@srcset` attributes. 

### `<ins>` Elements
`<ins>` HTML elements represents a range of text that has been added to a document.
You can use the `<del>` element to similarly represent a range of text that has been deleted from the document.

Supporting user agents may support `@cite` and `@datetime` on `<ins>` elements.

### `<kbd>` Elements
`<kbd>` HTML elements represents a span of inline text denoting textual user input from a keyboard, voice input, or any other text entry device.

### `<li>` Elements
The `<li>` HTML elements is used to represent an item in a list.
It must be contained in a parent element: an ordered list (`<ol>`), an unordered list (`<ul>`), or a menu (`<menu>`).

Supporting user agents may support `@value` attributes on `<li>` elements to specify their base 10 numeric conceptual index in the associated list as communicated to readers.
User agents should not reorder said list by these conceptual indexes.

### `<link>` Elements
`<link>` HTML elements specifies relationships between the current document and an external resource.
This element is most commonly used to link to CSS, but is also used to establish site icons (both "favicon" style icons and icons for the home screen and apps on mobile devices) among other things.
It should be a direct child of `<head>`.

Authors must include `@href` and `@rel` attributes on any `<link>` elements.
Supporting user agents must support `@href`, `@rel`, and `@type` attributes on `<link>` elements.
User agents may support `@as` (when `rel=preload`), `@cross-origin`, `@hreflang`, `@imagesizes` (when `rel=preload` and `as=image`, akin to `@sizes` on `<img>` elements), `@imagesrcset` (when `rel=preload` and `as=image`, akin to `@srcset` on `<img>` elements), `@integrity`, `@media`, `@prefetch`, `@referrerpolicy`, `@sizes`, and `@title`.

### The `<main>` Element
The `<main>` HTML element represents the dominant content of the body of a document.
The main content area consists of content that is directly related to or expands upon the central topic of a document, or the central functionality of an application.
There should be at most one `<main>` element without the `@hidden` attribute set on it, though more `<main>` elements may be present if they have the `@hidden` attribute set on them.

### `<map>` Elements
`<map>` HTML elements are used with `<area>` elements to define an "image map" (a clickable link area).
They must have a `@name` attribute, which must have the same value as the `@usemap` attribute on the image upon which it the image map should be overlaid.

They must contain exclusively one or more `<area>` elements defining the geometric regions of the referencing image to be made clickable.

Authors should avoid using this element to better cater to non-visual user agents. User agents may support this element.

#### `<area>` Elements
`<area>` HTML element defines an area inside an image map that has predefined clickable areas.
They must have `@shape`, `@coords`, `@href`, and `@href`-related properties.
The `@shape` and `@coords` attributes combine to describe the geometric region of the image readers may "click" to activate the link.

The interpretation of `@coords` depends on the value of `@shape`:

* `rect` indicates `@coords`'s numbers represents the number of pixels horizontally or vertically in from the image's top-left corner of the left, top, right, and bottom sides of the rectangle.
* `circle` indicates those numbers represents the x then y position of the circle's center followed by its radius.
* `poly` indicates the numbers represent alternate x then y coordinates representing the ordered corners of the polygon, with edges connecting consecutive points. And connecting to the final point to the first if they are not the same.
* `default` indicates user agents must ignore `@coords` and whether its present.

### `<mark>` Elements
`<mark>` HTML elements represents text which is marked or highlighted for reference or notation purposes, due to the marked passage's relevance or importance in the enclosing context.

### `<menu>` Elements
`<menu>` HTML element is a semantic alternative to `<ul>`.
It represents an unordered list of items (represented by `<li>` elements), each of these represent a link or other command that the user can activate.

All direct children must be `<li>` elements.

### `<meta>` Elements
`<meta>` HTML elements represents metadata that cannot be represented by other HTML meta-related elements, like `<base>`, `<link>`, `<script>`, `<style>` or `<title>`.

Authors must specify the "key" for this metadata record using the `@http-equiv` exclusive or `@name` attribute, and the "value" using the `@content` attribute.
If the key is specified using `@http-equiv` it should be the name of HTTP header.

Supporting user agents must support `@http-equiv`, `@name`, `@content` and should support `@charset` attributes on `<meta>` elements.

TBD source registries for @http-equiv or @name values.

### `<meter>` Elements
`<meter>` HTML elements represents either a scalar value within a known range or a fractional value.
It must have `@value`, `@low`, and `@high` attributes with base 10 numbers as values.
Authors may include fallback text for unsupporting browsers as the `<meter>` element's children.

Supporting user agents must support `@value`, `@low`, and `@high` attributes.
Supporting user agents may support `@min`, `@max`, `@optimum` attributes, in which case `@value` must be within the range between `@min` and `@max`.
`@value` should be within the range between `@low` and `@high`.

### `<nav>` Elements
`<nav>` HTML elements represents a section of a page whose purpose is to provide navigation links, either within the current document or to other documents.
Common examples of navigation sections are menus, tables of contents, and indexes.

### `<noscript>` Elements
`<noscript>` HTML element defines a section of HTML to be inserted if a user agent does not support `<script>` elements, or if said support is disabled.
If a user agent outright doesn't support `<script>` elements there's no need to handle `<noscript>` specially.

Authors must not use `<noscript>` to complain about a lack of JavaScript support.

### `<ol>` Elements
`<ol>` HTML elements represents an ordered list of items. All direct children must be `<li>` elements.

### `<p>` Elements
`<p>` HTML elements represents a paragraph.

### `<picture>` Elements
`<picture>` HTML elements contains zero or more `<source>` elements and one img element to offer alternative versions of an image for different display/device scenarios.
`<picture>` elements must contain exclusively `<source>` elements and up to one `<img>` element to use as a fallback.

#### `<source>` Elements
`<source>` HTML element specifies multiple media resources for the `<picture>`, `<audio>`, or `<video>` elements.
It is an empty element, meaning that it has no content and does not have a closing tag.
It is commonly used to offer the same media content in multiple file formats in order to provide compatibility with a broad range of browsers given their differing support for image file formats and media file formats.

`<source>` elements must be a direct child of a `<picture>`, `<audio>`, or `<video>` element.

### `<pre>` Elements
`<pre>` HTML elements represents preformatted text which is to be presented exactly as written in the HTML file.
Whitespace inside this element is displayed as written.

### `<q>` Elements
`<q>` HTML element indicates that the enclosed text is a short inline quotation.
This element is intended for short quotations that don't require paragraph breaks; for long quotations use the blockquote element.

### `<rp>` Elements
`<rp>` HTML elements are used to provide fall-back parentheses for browsers that do not support display of ruby annotations using the ruby element.
One `<rp>` element should enclose each of the opening and closing parentheses that wrap the `<rt>` element that contains the annotation's text.

### `<rt>` Elements
`<rt>` HTML element specifies the ruby text component of a ruby annotation, which is used to provide pronunciation, translation, or transliteration information for East Asian typography.
The `<rt>` element must always be contained within a ruby element.

### `<ruby>` Elements
`<ruby>` HTML element represents small annotations that are rendered above, below, or next to base text, usually used for showing the pronunciation of East Asian characters.
It can also be used for annotating other kinds of text, but this usage is less common.

### `<s>` Elements
`<s>` HTML elements renders text visually (if applicable) with a strikethrough, or a line through it. Use `<s>` elements to represent things that are no longer relevant or no longer accurate.
However, `<s>` is not appropriate when indicating document edits; for that, use the `<del>` and `<ins>` elements, as appropriate.

### `<samp>` Elements
`<samp>` HTML elements are used to enclose inline text which represents sample (or quoted) output from a computer program.

### `<script>` Elements
`<script>` is a reserved tagname for use by WHATWG.
Authors must not rely upon user agent support, though authors' use is allowed in HTMLite to overcome other shortcomings in supporting user agents.

`<script>` HTML elements (as defined by WHATWG) apply executable "scripts" to the document, either from its contained text or the linked resource to be embedded.

#### `<canvas>` Elements
`<canvas>` is a reserved tagname for use by WHATWG.
Authors should not use it.

`<canvas>` HTML elements (as defined by WHATWG) allow the embedding of dynamically-generated images, using the Canvas Scripting or WebGL APIs, from applied scripts into the document.

#### `<dialog>` Elements
`<dialog>` is a reserved tagname for use by WHATWG.
Authors should not use it.

`<dialog>` HTML elements (as defined by WHATWG) represents a dialog box or other interactive component, such as a dismissible alert, inspector, or subwindow.

#### `<output>` Elements
`<output>` is a reserved tagname for use by WHATWG.
Authors should not use it.

`<output>` HTML elements are a container element into which a site or app can inject the results of a calculation or the outcome of a user action.

#### `<portal>` Elements
`<portal>` is a reserved tagname for use by WHATWG.
Authors should not use it.

`<portal>` HTML element enables the embedding of another HTML page into the current one for the purposes of allowing smoother navigation into new pages.

#### `<progress>` Elements
`<progress>` is a reserved tagname for use by WHATWG.
Authors should not use it.

`<progress>` HTML elements displays an indicator showing the completion progress of a task, typically displayed as a progress bar.

#### `<slot>` Elements
`<slot>` is a reserved tagname for use by WHATWG.
Authors should not use it.

`<slot>` HTML elements are placeholders inside a web component that you can fill with your own markup, which lets you create separate DOM trees and present them together.

#### `<template>` Elements
`<template>` is a reserved tagname for use by WHATWG.
Authors should not use it.

`<template>` HTML element is a mechanism for holding HTML that is not to be rendered immediately when a page is loaded but may be instantiated subsequently during runtime using JavaScript.

### `<section>` Elements
`<section>` HTML elements represents a generic standalone section of a document, which doesn't have a more specific semantic element to represent it.
Sections should always have a heading, with very few exceptions.

### `<small>` Elements
`<small>` HTML elements represents side-comments and small print, like copyright and legal text, independent of its styled presentation.

### `<span>` Elements
`<span>` HTML elements are a generic inline container for phrasing content, which does not inherently represent anything.
It can be used to group elements for styling purposes (using the class or id attributes), or because they share attribute values, such as lang.
It should be used only when no other semantic element is appropriate.
`<span>` is very much like a `<div>` element, but in visual user agents `<div>` is a block-level element whereas a `<span>` is an inline element.

### `<strong>` Elements
`<strong>` HTML elements indicates that its contents have strong importance, seriousness, or urgency.

### `<sub>` Elements
`<sub>` HTML elements specifies inline text which should be displayed as subscript for solely typographical reasons.

### `<sup>` Elements
`<sup>` HTML element specifies inline text which is to be displayed as superscript for solely typographical reasons.

### `<style>` Elements
`<style>` HTML elements contains style information for a document, or part of a document.
It contains a stylesheet, which is applied to the contents of the document containing the `<style>` element.

Most supporting user agents only support stylesheets of the `text/css` content type, though that is not required.
User agents must imply any `<style>` elements lacking a `@type` attribute contains a `text/css` stylesheet.

Supporting user agents must support `@type`, `@media`, `@title`, and may support `@nonce` attributes on `<style>` elements.
If a `<style>` element has a `@title` element user agents must not apply it until or unless the reader says otherwise, making its stylesheet an "alternative stylesheet".

### `<table>` Elements
`<table>` HTML elements represents tabular data — that is, information presented in a two-dimensional grid comprised of "rows" and "columns" of "table cells" containing data.

Each row contains the same number of "slots", as does each column.
A table cell may "span" multiple contiguous rows and/or columns thus taking up multiple slots.

As direct children `<table>` elements must contain in order:

1. At most one `<caption>` element, may be absent.
2. Zero or more `<colgroup>` elements.
3. At most one `<thead>` element.
4. `<tr>` elements, a single `<tbody>` element, *exclusive or* nothing.
5. At most one `<tfoot>` element.

#### `<caption>` Elements
`<caption>` HTML elements specifies the caption (or title) of a `<table>`.
It must be a direct child of a `<table>` element without any other `<caption>` elements as direct children. 

#### `<colgroup>` Elements
`<colgroup>` HTML element defines a group of columns within a `<table>`.
It must be a direct child of a `<table>` element.
Supporting user agents must support `@span` (akin to `@colspan`) attributes on `<colgroup>` elements, in which case any children must be ignored.

`<colgroup>` elements can only contain `<col>` elements as direct children.

##### `<col>` Elements
`<col>` HTML elements defines a column within a table and is used for defining common semantics on all common cells.
It is generally found within a `<colgroup>` element.
Supporting user agents must support `@span` (akin to `@colspan`) attributes on `<colgroup>` elements.

#### `<tbody>` Elements
`<tbody>` HTML elements encapsulates a set of table rows (`<tr>` elements), indicating that they comprise the body of the table (`<table>`).
It must be a direct child of a `<table>` element without any `<tr>` or other `<tbody>` elements as direct children.

`<tbody>` elements must only contain `<tr>` elements as direct children.

#### `<tfoot>` Elements
`<tfoot>` HTML element defines a set of rows summarizing the columns of the table.
It must be a direct child of a `<table>` element without any other `<tfoot>` elements as direct children.

`<tfoot>` elements must only contain `<tr>` elements direct children.

#### `<thead>` Elements
`<thead>` HTML element defines a set of rows defining the head of the columns of the `<table>`.
It must be a direct child of a `<table>` element without any other `<thead>` elements as direct children.

`<thead>` elements must only contain `<tr>` elements as direct children.

#### `<tr>` Elements
`<tr>` HTML element defines a row of cells in a table.
The row's cells can then be established using a mix of `<td>` (data cell) and `<th>` (header cell) elements.
All direct children must be `<td>` or `<th>` elements.

##### `<td>` Elements
`<td>` HTML element defines a cell of a table that contains data.
It participates in the table model.
It must be a direct child of a `<tr>` elements.

Supporting user agents must support `@rowspan` and `@colspan` attributes on `<td>` elements, and may support `@headers` attributes.

##### `<th>` Elements
`<th>` HTML elements defines a cell as header of a group of table cells.
The exact nature of this group is defined by the `@scope` and `@headers` attributes.
It must be a direct child of a `<tr>` elements.

Supporting user agents must support `@rowspan` and `@colspan` attributes on `<td>` elements, and may support `@abbrev`, `@headers`, and `@scope` attributes.

### `<time>` Elements
`<time>` HTML elements represents a specific period in time.

Supporting user agents may support `@datetime` attributes on `<time>` elements, allowing them to implement additional features.

### The `<title>` Element
The `<title>` HTML element defines the document's title that is shown in e.g. a browser's title bar or a page's tab.
It only contains text; tags within the element are ignored.

### `<track>` Elements
`<track>` HTML elements are used as a direct child of the media elements, `<audio>` and `<video>`.
It lets you specify timed text tracks (or time-based data), for example to automatically handle subtitles.
The tracks are formatted in [WebVTT](https://w3c.github.io/webvtt/) format (.vtt files).
They must have `@src` and related attributes.

Supporting user agents must support `@src` and related attributes on `<track>` elements, and may support `@default` (to determine whether it is initially enabled), `@kind`, and `@label` attributes.

### `<u>` Elements
`<u>` HTML elements represents a span of inline text which should be rendered in a way that indicates that it has a non-textual annotation.

### `<ul>` Elements
`<ul>` HTML element represents an unordered list of items. All direct children must be `<li>` elements.

### `<var>` Elements
`<var>` HTML element represents the name of a variable in a mathematical expression or a programming context.

### `<video>` Elements
`<video>` HTML element references video resource to embed in the document, with any necessary playback controls.
You can use `<video>` for audio content as well, but the audio element may provide a more appropriate user experience.

Supporting user agents must support `@src` and related attributes on `<audio>` elements, and may support `<source>` elements, to specify which audio resource it plays.
Supporting user agents may support `@controls`, `@loop`, `@muted`, `@preload`, `@playsinline`, `@poster`, and possibly `@autoplay` attributes.

### `<wbr>` Elements
`<wbr>` HTML elements represents a word break opportunity — a position within text where the browser may optionally break a line, though its line-breaking rules would not otherwise create a break at that location.

## Chapter 4: Forms
Webforms provide a means of soliciting information from the reader, beyond which pages they wish to read.
And, without scripts messing with this paradigm, they can be used as a means for user agents to grant access to that information without triggering confirmation blindness in the reader.
Interactive user agents should support webforms, non-interactive user agents may support webforms.

User agents at the disgression may render webform controls inline where they appear in the markup, or extract the webform into a seperate webpage rendered to better suite the device or platform.
HTMLite places constraints on authors so user agents can render webforms out-of-line from the embedding webpage.

As with the rest of HTMLite, all webforms elements and attribute are in the namespace `http://www.w3.org/1999/xhtml`.

### `<form>` Elements
`<form>` elements groups a related set of controls (descendant elements defined in this chapter), specifying where the data entered into them should be sent.
This data must only be sent when readers request it, and user agents may block the request if any tests requested by "validation attributes" fails.

Supporting user agents must support `@action`, `@method`, `@enctype`, `@name`,  attributes on `<form>` elements to identify where the data should be sent, and may support any or all of the `@href`-related attributes apart from `@href` itself as well as `@novalidate` and `@autocomplete`.

#### `@action` Attributes
An `@action` attribute on `<form>` elements specifies the relative URI to which the form data (as per "Serializing Form Data") is sent upon reader-request once validated.

#### `@method` Attributes
An `@method` attribute on `<form>` elements specifies the HTTP method which should be used when sending the form data. Other URI schemes than HTTP may interpret this attribute as they wish.

#### `@enctype` Attributes
Specifies the MIMEtype of the encoding which should be used to send the form data to the specified URI.
Supporting user agents must support values of `application/x-www-form-url-encoded`,  `multipart/form-data`, or `text/plain`.
Absence of an `@enctype` attribute is equivalent to `enctype=application/x-www-form-url-encoded`.
Authors should set the value to `multipart/form-data` if they have any `file` inputs in the form.

#### `@novalidate` Attributes
A `@novalidate` attribute on `<form>` elements indicates the user agent should skip the validation step when the reader submits the webform.
Always sending the data to its author-specified destination regardless of whether it passes the author's validation rules.

#### `@autocomplete` Attributes
Indicates the author would prefer that the user agent doesn't suggest values for form inputs, which readers can select between, from non-author-provided sources.

### `<input>` Elements
`<input>` elements represents a "form control" into which readers can enter some data to be sent to the author-specified destination when its form is submitted.
`<input>` elements must either be a descendant of a `<form>` element or have the value of its `@form` attribute match the value of a `<form>` element's `@name` attribute in the document.
If both cases are true for an `<input>` element the `@form` attribute takes precedance in determining which `<form>` element this `<input>` element belongs to.

Supporting user agents must support `@name`, `@value`, `@type`, `@form`, and any other requirements specified for the value of `@type` on `<input>` elements.
Supporting user agents may support validation attributes of `@max`, `@maxlength`, `@min`, `@minlength`, `@pattern`, and `@required`.
Supporting user agents may also support the properties `@autocomplete`, `@autofocus`, `@disabled`, `@list`, `@readonly`, and `@size`.

All values of `@type` are supported on `<input>` elements, with a fallback of "text".

### `<button>` Elements
`<button>` elements represents a control for triggering some action on its `<form>`.
Like `<input>` it must be a descendant of a `<form>` element or have a valid `@form` attribute, to identify its `<form>`.
The difference from `<input>` elements is that authors have the full power of HTML with which to label their `<button>` elements.

Supporting user agents must support `@name`, `@value`, `@type`, and `@form` attributes on `<button>` elements, and may support `@autofocus`, `@disabled`, `@formaction`, `@formenctype`, `@formmethod`, `@formnovalidate`, and `@formtarget` attributes.
The value of `@type` must be "submit", "reset", or "button" with a fallback of "submit".

### `<textarea>` Elements
`<textarea>` HTML elements represents a multi-line plain-text editing control, useful when you want to allow users to enter a sizeable amount of free-form text.
Like `<input>` it must be a descendant of a `<form>` element or have a valid `@form` attribute, to identify its `<form>`.
It must only contain plaintext with no descendant elements, to serve as the value to be sent to the author-defined destination when the reader submits the form.

Supporting user agents must support `@name` and `@form` attributes on `<textarea>` elements, and may support `@autocomplete`, `@autofocus`, `@disabled`, `@placeholder`, `@readonly`, and `@spellcheck` attributes.
Supporting user agents may also support the `@maxlength`, `@minlength`, `@required` validation attributes, as well as the `@cols`, `@rows`, and `@wrap` visual presentation attributes.

#### `@cols` Attributes
`@cols` attributes on `<textarea>` elements indicates a base 10 number of how many characters wide the `<textarea>` should be rendered.

#### `@rows` Attributes
`@rows` attributes on `<textarea>` elements indicates a base 10 number of how many characters tall the `<textarea>` should be rendered.

#### `@wrap` Attributes
`@wrap` attributes on `<textarea>` elements indicates whether line wrapping should be applied within the `<textarea>` element, and whether newline characters should be inserted to apply that linewrapping to the value sent to the service when the form is submitted.

A value of "hard" indicates the inner text should be line wrapped when its line gets to long to be rendered inside the `<textarea>`, and additional newline characters should be added to reflect that in the value sent to the service.

A value of "soft" indicates the inner text should be line wrapped when its line gets to long to be rendered inside the `<textarea>`, but no additional newline characters should be added to the value sent to the service.

A value of "off" indicates no line wrapping should be applied, instead adding an interactive control to reach the offscreen text.

### `<select>` Elements
`<select>` HTML elements represents a control that provides a menu of options from which the reader can select between.
Like `<input>` it must be a descendant of a `<form>` element or have a valid `@form` attribute, to identify its `<form>`.
It must only contain `<option>` or `<optgroup>` elements as direct children.

Supporting user agents must support `@name`, `@multiple`, and `@form` attributes on `<select>` elements, and may support the `@autocomplete`, `@autofocus`, and `@disabled` attributes.
They may also support the `@required` validation attribute and/or the `@size` visual presentation attribute.

#### `<optgroup>` Elements
`<optgroup>` HTML elements creates a grouping of options within a `<select>` element.
They must only contain `<option>` elements as direct children, and must be a direct child of a `<select>` element.
They must have a `@label` attribute with a human-readable value.

Supporting user agents must support `@label` attributes on `<optgroup>` elements, and may support `@disabled` attributes to indicate whether all child `<options>` are also disabled.

#### `<option>` Elements
`<option>` HTML elements represents an individual value a reader can select for a `<select>` element.
It must be a direct child of a `<select>` or `<optgroup>` element, or a descendant of a `<datalist>` element.
It must only contain plaintext with no descendant elements.

Supporting user agents must support `@label`, `@value`, and `@selected` attributes, and may support `@disabled`.
If the `@label` attribute is absent its value must be taken from the `<option>` element's contained text, as per the `@value` attribute.

The value of the `<select>` element must be that of the `@value` attribute of all its descendant `<option>` element(s) selected by the reader, defaulting to those with the `@selected` attribute.
If the `<select>` doesn't have a `@multiple` attribute the user agent must enforce that only one of its descendant `<option>` elements can be selected.
The value of an `<option>` element cannot be editted by the reader, only selected.

##### `@selected` Attributes
The presence of a `@selected` attribute on `<option>` elements indicates its value should be the default one(s) for the ancestor `<select>` element.
The reader may select a different value by selecting another `<option>` element, or unselecting this one.

### `<keygen>` Elements
`<keygen>` is deprecated by WHATWG but not HTMLite.

`<keygen>` elements provides a form control by which a reader can authenticate into a supporting service cryptographically, such that neither has to worry in the unfortunately likely case external attackers exfiltrate the service's accounts database that those attackers can authenticate as the reader on this or other services.
Like `<input>` it must be a descendant of a `<form>` element or have a valid `@form` attribute, to identify its `<form>`.
`<keygen>` elements must have a `@name` attribute and should have `@challenge` and `@keytype` attributes.

Supporting user agents must support `@name`, `@challenge`, `@keytype`, `@keyparams`/`@pqg`, and `@form` attributes, and may support `@autofocus` and `@disabled` attributes.
`@keyparams` and `@pqg` are synonyms, that is `keyparams=value` and `qpg=value` has the same effect.
Supporting user agents must warn when the challenge response will be sent over an unencrypted & non-authenticated protocol, or to another host then the one from which this document was downloaded from.
Supports should warn if the document wasn't downloaded from the same authenticated encrypted protocol the form will be submitted over.

TBD: I don't know what I'm doing when writing crypto standards.

#### `@challenge` Attributes
The `@challenge` attribute on `<keygen>` elements adds additional text to be cryptographically signed, to secure against replay attacks. An empty or absent `@challenge` attribute adds no additional text.

For `@challenge` to serve its purpose authors should ensure a new cryptographically random value is generated each time the page is downloaded, which the service knows about.
The service must accept only the first valid response to a given `@challenge` value.

#### `@keytype` and `@keyparams`/`@pqg` Attributes
TBD: Cite crypto standards for what these values mean, and the what the value of `@keyparams` means.

The `@keytype` indicates which cryptographic algorithm should be used to generate a cryptographic keypair and encrypt/decrypt text with them.
It may have values of "RSA", "DSA", or "EC".
A `@keyparams` or `@qpg` attribute is required for values of "DSA" or "EC", but ignored for a value of "RSA".

#### Generating the value
TBD: Cite referenced crypto standards. Below is copied from MDN.

Upon form submission, using the following datastructures:

        PublicKeyAndChallenge ::= SEQUENCE {
            spki SubjectPublicKeyInfo,
            challenge IA5STRING
        }
        SignedPublicKeyAndChallenge ::= SEQUENCE {
            publicKeyAndChallenge PublicKeyAndChallenge,
            signatureAlgorithm AlgorithmIdentifier,
            signature BIT STRING
        }

The selected or generated public key and challenge string (from the `@challenge` attribute) are DER encoded as `PublicKeyAndChallenge`, and then digitally signed with the private key to produce a `SignedPublicKeyAndChallenge`.
The `SignedPublicKeyAndChallenge` is Base64 encoded, and the ASCII data is finally submitted with the rest of the form to the server as the value of a form name/value pair.

### `<label>` Elements
`<label>` elements contain the textual label for a form control.
`<label>` elements must either have a single descendant `<input>`, `<button>`, `<textarea>`, or `<select>` element (collectively referred to as "form controls"), or it must have a `@for` which shares its value with that of an `@id` attribute on a form control.
If both cases hold for a `<label>` element the `@for` attribute takes precedance in identifying the "associated form control".
`<label>` elements should not contain "interactive" elements as descendants, such as anything with an `@href` attribute.

Authors must ensure every form control has a `<label>` element associated with it.
Supporting user agents must support `@for` attributes on `<label>` elements.

#### `@for` Attributes
`@for` attributes on `<label>` elements overrides which form control it is associated with.
The value of `@for` must be the same as the value of the `@id` attribute of a form control, this form control will be the one this `<label>` element (which the `@for` attribute is attached to) is associated with.

### `<fieldset>` Elements
`<fieldset>` elements groups related form controls and their associated labels.
`<fieldset>` elements should have a `<legend>` element as its first child to describe how those form controls are related.

Supporting user agents must support `@form` and `@name` attributes on `<fieldset>` elements, and may support `@disabled`.
All these attributes should be treat as if they were set on all descendant form controls.

#### `<legend>` Elements
`<legend>` elements describes to the reader how all the form controls in its parent `<fieldset>` element are related.
`<legend>` elements must have a `<fieldset>` element as its parent.

### `@name` Attributes
`@name` attributes on form controls specifies the text which should identify this form control to the server once the reader has submitted the form.

`@name` attributes on `<form>` elements must have a unique value amongst all `<form>` elements in the document.
Allowing form controls to reference the `<form>` element by name.

### `@value` Attributes
`@value` attributes on form controls specifies the initial value of the form control.
Supporting user agents must let readers alter the value of a form control away from the value of `@value` unless this specification says otherwise, and must send it paired with the value of the same form control's `@name` attribute to the author-specified destination when the reader submits the form.

### `@type` Attributes
`@type` attributes on form controls specifies what kind of values can be validly entered into this form control, with or without the user agent providing the reader interactive aids.

User agents may support whichever of the following values for `@type` they wish.
Any invalid or unsupported value must be treated as if the `@type` was set to "text" for `<input>` elements, or "submit" for `<button>` elements.
The value required for each `@type` value does not preclude user agents from displaying a localized representation to the reader.

#### `type=button`
"button" is a reserved value for use by WHATWG.
User agents which don't support `<script>` elements may treat such `<input>` or `<button>` elements as if they weren't on the page, or they may render them with no real interactivity.

A form control for applied scripts to respond to the activation of. Supporting user agents should render these with the value of the `@value` attribute, empty by default.
The value of its `@name` and `@value` attributes should not be submitted with the form.

#### `type=checkbox`
A check box allowing single values to be selected/deselected.
Whether this form control is initially selected is controlled by the presence or absence of the `@checked` attribute.

Supporting user agents must submit the values of the `@name` and `@value` attributes with the rest of the form if it is currently selected, though it must not submit them if the form control is currently deselected.
Supporting user agents must not allow readers to edit the value of this form control, only select and deselect it.

Supporting user agents must support `@checked` attributes on form controls of type "checkbox".

#### `type=color`
Indicates the value must be 3 2-digit base 16 numbers concatenated together without any seperating characters, prefixed by a hash (`#`) character.
Representing in order the red, green, and blue components to be mixed together additively (as would happen when sending those bytes to certain monitors) to form a colour akin to [CSS syntax](https://www.w3.org/TR/CSS22/syndata.html#color-units).

Supporting user agents should not make the reader aware of these technical details, allowing them to think in terms of selecting a colour.

#### `type=date`
The value must be valid according to ISO8601, that is in a "YYYY-MM-DD" format with each letter representing a base 10 digit.
Where the year (`Y`), month (`M`), and day (`D`) are in the gregorian calendar.

Supporting validating user agents must support the `@max` and `@min` validation attributes on `type=date` form controls, though their values should be dates of this format rather than base 10 numbers.
`@step` is also supported, being interpreted as a quantity of days.

#### `type=datetime-local`
A form control for entering a date and time with no time zone according to ISO8601, that is in a "YYYY-MM-DDThh:mm" format with each letter (apart from `T`) representing a base 10 digit.
Where the year (`Y`), month (`M`), day (`D`), hour (`h`), and minute (`m`) are in the gregorian calendar.

Supporting user agents may support the `@max` and `@min` validation attributes on `type=date` form controls, though their values should be dates of this format rather than base 10 numbers.
Supporting user agents may also support the `@step`, being interpreted as a quantity of seconds.

#### `type=email`
A form control for editing an email address.

TBD: Cite the spec for email address format.

Supporting user agents must support the `@multiple` attribute on form controls of type "email", and may support the `@list`, `@placeholder`, and `@readonly` attributes.
Supporting user agents must also support the `@maxlength`, `@minlength`, and/or `@pattern` validating attributes, and/or the `@size` visual property.
If the `@multiple` attribute is present on the form control each email address must be seperated in the value to be submitted to the server by whitespace and/or comma (`,`) character(s).

#### `type=file`
A form control that lets the user select one or more file(s) typically from their local device.
Their values should be a fake filepath to the uploaded file, with the files themselves being sent as a seperate component of the form data where the serialization allows them.

Supporting user agents must support `@multiple` attribute on form controls of type "file", should support `@accept` validating attributes, and may support `@capture` attributes.

##### `@accept` Attributes
`@accept` attributes on form controls of type "file" specifies a comma (`,`) seperated list of MIMEtypes and file extensions identifying the valid filetypes which may be uploaded via this form control to the service.
A file extension must start with a period (`.`) to indicate a possible suffix (including said `.`) for uploaded files.

Where a filetype is identified by multiple MIMEtypes and/or file extensions authors should list all those MIMEtypes and file extensions, in case the user agent isn't aware they're synonyms.

##### `@capture` Attributes
`@capture` attributes on form controls of type "file" specifies that supporting user agents should offer an option for taking a capturing a new photo file from a camera attached to the device, and which camera should be used if there's multiple.
User agents may provide this option regardless of the presence, or support for, this attribute.
`@capture` attributes may have a value of "user" or "environment".

If the value's "user" user agents should use a frontfacing camera useful for taking selfies.
If the value's "environment" user agents should use a backfacing camera.

#### `type=hidden`
Form controls of type "hidden" holds data to be echoed back to the service once the reader submits the form.
Readers may not edit their values, and user agents which render forms inline with the embedding webpage should not render these form controls.

If the value of `@name` is set to "_charset_" the user agent should set the form control's value to the name of the text encoding used to submit the form.

#### `type=image`
Form controls of type "image" behaves like ones of type "submit", with the difference they are rendered using the linked image resource to be embedded with a fallback of `@alt`.

Supporting user agents must support `@src` and related attributes on form controls of type "image", and may support `@formaction`, `@formenctype`, `@formmethod`, `@formnovalidate`, and `@formtarget`.

#### `type=month`
A form control for entering a month and year with no time zone according to ISO6501, that is in "YYYY-MM" format with each letter representing a base 10 digit.
Where the year (`Y`) and month (`M`) are in the gregorian calendar.

Supporting user agents may support the `@max` and `@min` validation attributes on `type=month` form controls, though their values should be dates of this format rather than base 10 numbers.
Supporting user agents may also support the `@step`, being interpreted as a quantity of months.

#### `type=number`
A form control for entering a base 10 number.

Supporting user agents may support the `@max`, `@min`, and `@step` validation attributes form controls of type "number". 

#### `type=password`
A single-line text form control whose value is obscured from others, apart from the reader, who can also perceive the user agent's output.
User agents should warn readers when they would be sending this value over the network unencrypted and/or non-authenticated.

#### `type=radio`
Form controls of type "radio" behaves similarly to those of type "checkbox", but only one form control of type "radio" in the same form with the same value for `@name` may be selected.
And they can't typically be deselected.

Supporting user agents must support `@checked` attributes on form controls of type "radio".

#### `type=range`
A form control for entering a number whose exact value is not important.
They should have `@min`, `@max`, and `@value` attributes. User agents must treat the absence of `@min` as holding the value 0, the absence of `@max` as holding the value 100, and the absence of `@value` as holding the value halfway between `@min` and `@max`.

Supporting user agents must support `@min`, `@max`, and may support `@step`.

#### `type=reset`
A form control that resets all other form controls in the same form to default values as specified by `@value`, `@checked`, & `@selected`.

Supporting user agents should render these with the value of the `@value` attribute, defaulting to the localized equivalent text of "Reset".
The value of its `@name` and `@value` attributes should not be submitted with the form.

#### `type=search`
A single-line text form control for entering search queries.
Behaves equivalent to form controls of type "text" but may render differently, and may include a button for deleting all existing text in the form control.

Supporting user agents may support `@spellcheck` attributes on form controls of type "search".

#### `type=submit`
A form control for submitting its form once activated.
If the form was submitted by activating this "submit" control, as opposed to another "submit" control or a user agent facility, and it has a `@name` attribute, append its `@name`/`@value` pair to the data submitted server. Otherwise omit this control from the data being submitted.

Supporting user agents may support the `@formaction`, `@formenctype`, `@formmethod`, `@formnovalidate`, and `@formtarget` attributes on form controls of type "submit".

#### `type=tel`
A form control for entering telephone numbers.
No format validation is performed unless otherwise specified by the author, though the input may be constrained to digit and punctuation characters.

#### `type=text`
A form control for entering single-line text.
No format validation is performed unless otherwise specified by the author.

Supporting user agents may support `@spellcheck` attributes on form controls of type "text".

#### `type=time`
A control for entering a time value with no time zone according to ISO8601, that is in "hh:mm" format where all letters represents a base 10 digit.
Where the hours (`h`) and minutes (`m`) are in 24-hour time.

Supporting user agents may support the `@max` and `@min` validation attributes on `type=month` form controls, though their values should be dates of this format rather than base 10 numbers.
Supporting user agents may also support the `@step`, being interpreted as a quantity of seconds.

#### `type=url`
A control for entering a URI.

Supporting user agents may support `@spellcheck` attributes on form controls of type "url".

#### `type=week`
A control for entering a date consisting of a week-year number and a week number with no time zone according to ISO8601, that is in "YYYY-Www" format with each letter (apart from `W`) representing a base 10 digit.
The year (`Y`) and week (`w`) are in the gregorian calendar.

Supporting user agents may support the `@max` and `@min` validation attributes on `type=month` form controls, though their values should be dates of this format rather than base 10 numbers.
Supporting user agents may also support the `@step`, being interpreted as a quantity of weeks.

### `@form` Attributes
`@form` attributes on form controls overrides which `<form>` element it is associated with.
The value of `@form` must be the same as that of the `@name` attribute of a `<form>` element, this `<form>` element will be the one this form control (which the `@form` attribute is attached to) is associated with.

### `@multiple` Attributes
The presence of an `@multiple` attribute on a form control indicates multiple values may be entered via the form controls, rather than just one.
When uploading this information each value will become record, each paired with the value of the `@name` attribute.

### `@placeholder` Attributes
`@placeholder` attributes on form controls provides a sample or hint indicating what value should be entered into the form control, to be rendered when no information has yet to be entered.

### `@spellcheck` Attributes
`@spellcheck` attributes on textual form controls indicates whether the user agent should highlight words which it suspects are misspelled, possibly offering to correct that spelling.

`@spellcheck` may have values of "yes", "no", or "default".
"yes" indicates supporting user agents should spellcheck, "no" indicates it should not, and "default" indicates it may.

### `@size` Attributes
The `@size` attribute on form controls specifies a base 10 number of characters (or for `<select>` elements, descendant `<option>` elements) visual user agents should ensure are visible at a time, when the compute the size at which to render this element.
Visual user agents may ignore this attribute if `@multiple` isn't applied to the same `<select>` element.

### `@list` Attributes
`@list` attributes references a `<datalist>` element by its `@id` attribute, to provide author-provided autocompletions augmenting any user agent-provided ones.
Supporting user agents should support `@list` attributes on `<input>` and `<textarea>` elements, depending on the value of that `<input>` element's `@type` attribute.

#### `<datalist>` Elements
`<datalist>` HTML element contains a set of `<option>` elements that represent the permissible or recommended options available for readers to choose from within other controls.
`<datalist>` must have an `@id` attribute.

`<datalist>` must contain `<option>` elements as descendants.
Authors may add additional markup, including surrounding `<select>` element(s), to cater towards unsupporting user agents; supporting user agents must ignore any such contained markup apart from the descandant `<option>` elements.

### `@autocomplete` Attributes
`@autocomplete` on form controls indicates where the user agent should source autocompletion options, and whether it should.
One possible source for most values of `@autocomplete` are the values previously entered on other sites for form controls with the same `@autocomplete` value.
User agents may support any of the below values of `autocomplete` as they wish.

`@autocomplete` on `<form>` elements indicates the default value of `@autocomplete` for all its form controls.
On `<form>` elements `@autocomplete` must have a value of "yes" or "no", where empty or absent values indicates the default be "yes".

TBD: Is there a registry for this? Should we create one? https://html.spec.whatwg.org/multipage/#attr-fe-autocomplete

#### `autocomplete=off`
Indicates the author would prefer if the user agent didn't offer the reader any autocompletions.

#### `autocomplete=on`
Indicates, despite any intentions stated on the `<form>` element itself, that user agents should provide autocompletions.

#### `autocomplete=name`
The field expects the value to be a person's full name.

##### `autocomplete=honorific-prefix`
The prefix or title, such as "Mrs.", "Mr.", "Miss", "Ms.", "Dr.", or "Mlle.".

Authors should instead use `autocomplete=name` to better cater towards readers whose names doesn't split cleanly into this structure.

##### `autocomplete=given-name`
The given (or "first") name.

Authors should instead use `autocomplete=name` to better cater towards readers whose names doesn't split cleanly into this structure.

##### `autocomplete=additional-name`
The middle name.

Authors should instead use `autocomplete=name` to better cater towards readers whose names doesn't split cleanly into this structure.

##### `autocomplete=family-name`
The family (or "last") name.

Authors should instead use `autocomplete=name` to better cater towards readers whose names doesn't split cleanly into this structure.

##### `autocomplete=honeric-suffix`
The suffix, such as "Jr.", "B.Sc.", "PhD.", "MBASW", or "IV".

Authors should instead use `autocomplete=name` to better cater towards readers whose names doesn't split cleanly into this structure.

##### `autocomplete=nickname`
A nickname or handle.

Authors should instead use `autocomplete=name` to better cater towards readers whose names doesn't split cleanly into this structure.

#### `autocomplete=email`
An email address.

TBD: Cite SMTP spec.

#### `autocomplete=username`
A username or account name, by which the reader is identified by the service and its audience.

#### `autocomplete=new-password`
A new password.
When creating a new account or changing passwords, authors should use this for an "Enter your new password" or "Confirm new password" field, as opposed to a general "Enter your current password" field that might be present.
This may be used by the user agent both to avoid accidentally filling in an existing password and to offer assistance in creating a secure password.

#### `autocomplete=current-password`
The user's current password.
To discourage password reuse (leading to security vulnerabilities) if these autocompletions are not taken from the reader's password vault (provided by the operating system), no autocompletions should be given for controls with `autocomplete=current-password`.

#### `autocomplete=one-time-code`
A one-time code used for verifying user identity.

TBD: What's the user agent supposed to do with this?

#### `autocomplete=organization-title`
A job title, or the title a person has within an organization, such as "Senior Technical Writer", "President", or "Assistant Troop Leader".

#### `autocomplete=organization`
A company or organization name, such as "Acme Widget Company" or "Girl Scouts of America".

#### `autocomplete=street-address`
A street address.
This can be multiple lines of text, and should fully identify the location of the address within its second administrative level (typically a city or town), but should not include the city name, ZIP or postal code, or country name.

##### `autocomplete-street-address-line1`, `autocomplete=street-address-line2`, `autocomplete=street-address-line3`
Each individual line of the street address.
These should only be present if the "street-address" is not present in the form.

##### `autocomplete=address-level4`
The finest-grained administrative level, in addresses which have four levels.

##### `autocomplete=address-level3`
The third administrative level, in addresses with at least three administrative levels.

##### `autocomplete=address-level2`
The second administrative level, in addresses with at least two of them.
In countries with two administrative levels, this would typically be the city, town, village, or other locality in which the address is located.

##### `autocomplete=address-level1`
The first administrative level in the address.
This is typically the province in which the address is located.
In the United States, this would be the state.
In Switzerland, the canton.
In the United Kingdom, the post town.

##### `autocomplete=country`
A country or territory code.

##### `autocomplete=country-name`
A country or territory name.

##### `autocomplete=postal-code`
A postal code (in the United States, this is the ZIP code).

#### `autocomplete=cc-name`
The full name as printed on or associated with a payment instrument such as a credit card.

##### `autocomplete=cc-given-name`
A given (first) name as given on a payment instrument like a credit card.

Authors should instead use `autocomplete=cc-name` to better cater towards readers whose names doesn't split cleanly into this structure.

##### `autocomplete=cc-additional-name`
A middle name as given on a payment instrument or credit card.

Authors should instead use `autocomplete=cc-name` to better cater towards readers whose names doesn't split cleanly into this structure.

##### `autocomplete=cc-family-name`
A family name, as given on a credit card.

Authors should instead use `autocomplete=cc-name` to better cater towards readers whose names doesn't split cleanly into this structure.

#### `autocomplete=cc-number`
A credit card number or other number identifying a payment method, such as an account number.

#### `autocomplete=cc-exp`
A payment method expiration date, typically in the form "MM/YY" or "MM/YYYY".

##### `autocomplete=cc-exp-month`
The month in which the payment method expires.

##### `autocomplete=cc-exp-year`
The year in which the payment method expires.

#### `autocomplete=cc-csc`
The security code for the payment instrument; on credit cards, this is the 3-digit verification number on the back of the card.

#### `autocomplete=cc-type`
The type of payment instrument (such as "Visa" or "Master Card").

#### `autocomplete=transaction-currency`
The currency in which the transaction is to take place.

#### `autocomplete=transaction-amount`
The amount, given in the currency specified by "transaction-currency", of the transaction, for a payment form.

#### `autocomplete=language`
A preferred language, given as a valid [BCP 47 language tag](https://www.rfc-editor.org/info/bcp47).

#### `autocomplete=bday`
A birth date, as a full ISO 639 date.

##### `autocomplete=bday-day`
The day of the month of a birth date.

##### `autocomplete=bday-month`
The month of the year of a birth date.

##### `autocomplete=bday-year`
The year of a birth date.

#### `autocomplete=sex`
A gender identity (such as "Female", "Fa'afafine", "Male"), as freeform text without newlines.

#### `autocomplete=tel`
A full telephone number, including the country code.

##### `autocomplete=tel-country-code`
The country code, such as "1" for the United States, Canada, and other areas in North America and parts of the Caribbean.

##### `autocomplete=tel-national`
The entire phone number without the country code component, including a country-internal prefix.
For the phone number "1-855-555-6502", this field's value would be "855-555-6502".

##### `autocomplete=tel-area-code`
The area code, with any country-internal prefix applied if appropriate.

##### `autocomplete=tel-local`
The phone number without the country or area code.
This can be split further into two parts, for phone numbers which have an exchange number and then a number within the exchange.
For the phone number "555-6502", use "tel-local-prefix" for "555" and "tel-local-suffix" for "6502".

#### `autocomplete=tel-extension`
A telephone extension code within the phone number, such as a room or suite number in a hotel or an office extension in a company.

#### `autocomplete=impp`
A URL for an instant messaging protocol endpoint, such as "xmpp:username@example.net".

#### `autocomplete=url`
A URL, such as a home page or company web site address as appropriate given the context of the other fields in the form.

#### `autocomplete=photo`
The URL of an image representing the person, company, or contact information given in the other fields in the form.

### `@autofocus` Attributes
The presence of a `@autofocus` attribute on a form control indicates that control should receive should receive the keyboard, or equivalent, input by default upon page load.

### `@disabled` Attributes
The presence of a `@disabled` attribute on a form control indicates that the reader may not alter its value for temporary reasons.

### `@readonly` Attributes
The presence of a `@readonly` attribute on a form control indicates that the reader may not alter its value for more permannt reasons.

### `@formaction` Attributes
`@formaction` attributes on form controls overrides the value of its associated `<form>` element's `@action` attribute when the form is submitted via this form control.

### `@formenctype` Attributes
`@formenctype` attributes on form controls overrides the value of its associated `<form>` element's `@enctype` attribute when the form is submitted via this form control.

### `@formmethod` Attributes
`@formmethod` attributes on form controls overrides the value of its associated `<form>` element's `@method` attribute when the form is submitted via this form control.

### `@formnovalidate` Attributes
`@formnovalidate` attributes on form controls overrides the value of its associated `<form>` element's `@novalidate` attribute when the form is submitted via this form control.

### `@formtarget` Attributes
`@formtarget` attributes on form controls overrides the value of its associated `<form>` element's `@target` attribute when the form is submitted via this form control.

### Validation attributes
User agents may implement a validation step so readers can be informed when the data they entered to be sent to the destination is invalid, without needing to wait for that destination to tell them that over the network.
The service receiving the form submission should perform its own validation both to cater towards user agents which don't implement webform validation, and to protect against attackers who can't be constrained by the HTML.

#### `@max` Attributes
`@max` attributes on numeric form controls specifies the maximum base 10 number (inclusive) this form control can validly hold.
Any numeric value greater than this is invalid, and should block form submission.

#### `@min` Attributes
`@min` attributes on numeric form controls specifies the minimum base 10 number (inclusive) this form control can validly hold.
Any numeric value lower than this is invalid, and should block form submission.

#### `@step` Attributes
`@step` attributes on numeric form controls specifies the base 10 numeric difference between any two valid values.
The value of `@min`, if present, is always a valid value from which all others can be derived.
Otherwise serves as a constraint on interactivity.

#### `@maxlength` Attributes
`@maxlength` attributes on form controls specifies the maximum number of characters as a base 10 number (inclusive) this form control's value can consist of.
If the textual value has more characters (including repeats) then this is invalid, and should block form submission.

#### `@minlength` Attributes
`@minlength` attributes on form controls specifies the minimum number of characters as a base 10 number (inclusive) this form control's value can consist of.
If the textual value has fewer characters then this is invalid, and should block form submission.

#### `@pattern` Attributes
`@pattern` attributes on form controls specifies a regular expression which the form control's value must match to be considered valid.
A value which does not match this regular expression should block form submission.

TBD: Source regexp specs.

#### `@required` Attributes
The presence of a `@required` attribute on a form control indicates that empty or absent values are considered invalid and should block form submission.

### Serializing Form Data
To submit a form to a server, its data must be serialized over the network.
HTMLite defines how to serialize this data over the network into 3 different resource formats.

The form data to be serialized is gathered into a list of name/value pairs, where both name and value are textual data.
If there's multiple values for a single form control (possibly apart from those of type "email") or if there's multiple form controls of the same name, there will be pairs in this list with the same name.
Form controls of type "file" reference additional data to be included into this serialization.

Unless otherwise stated, for HTTP(S) the serialized form data should be sent in the request body, with the appropriate `Content-Type` HTTP header set.

#### `application/x-www-form-url-encoded`
To serialize form data as `application/x-www-form-url-encoded` user agents must escape any non-ASCII, whitespace, hash (`#`), ampersand (`&`), or equals (`=`) characters in the name or value component of each name/value pair, concatenate the two seperated by an equals character, and concatenate that textual representation of each key/value pair with ampersand (`&`) characters.

If the HTTP method to be used is "GET" incorporate this serialized form data into the requested URL as its query component, between the `?` and the `#`.

#### `multipart/form-data`
To serialize form data as `multipart/form-data` user agents must serialize the form data, and any referenced files, in conformance to [RFC 1467](https://www.rfc-editor.org/rfc/rfc1867.txt)

#### `text/plain`
To serialize form data as `application/x-wwww-form-url-encoded` concatenate all the values from the form-value pairs seperated by newlines.

**NOTE** This is mostly defined for DEBUG purposes, it isn't intended to be that useful.

## Appendix A: Suggested Error Correction Algorithm
The behaviour of closing tags in the HTML syntax whose tagname differs from the current element is undefined behaviour.
Due to the rarity of these "unbalanced tags" on real world webpages, this appendix describes an algorithm user agents may use to parse a halfway decent document tree from some poorly structured HTML source code.
This algorithm must not be applied to XML documents.

This algorithm is: Upon encountering an invalid closing tag, check if an element of the same tagname is in the open tag stack.
If it is pop and implicitly close each element in the stack until you've popped and closed that element.
If it isn't (say, because it has already been implicitly closed) ignore this closing tag.

Other, more common, parsing quirks are described in "Chapter 1: Parsing" and are normative.

## Appendix B: Default CSS Stylesheet
HTMLite specifically avoids mandating how elements should be rendered visually, however this appendix suggests some default visuals written in machine-readable CSS.
Consider this appendix illustrative rather than normative.

        html, address,
        blockquote,
        body, dd, div,
        dl, dt, fieldset, form,
        frame, frameset,
        h1, h2, h3, h4,
        h5, h6, noframes,
        ol, p, ul, center,
        dir, hr, menu, pre   { display: block; unicode-bidi: embed }
        li              { display: list-item }
        head            { display: none }
        table           { display: table }
        tr              { display: table-row }
        thead           { display: table-header-group }
        tbody           { display: table-row-group }
        tfoot           { display: table-footer-group }
        col             { display: table-column }
        colgroup        { display: table-column-group }
        td, th          { display: table-cell }
        caption         { display: table-caption }
        th              { font-weight: bolder; text-align: center }
        caption         { text-align: center }
        body            { margin: 8px }
        h1              { font-size: 2em; margin: .67em 0 }
        h2              { font-size: 1.5em; margin: .75em 0 }
        h3              { font-size: 1.17em; margin: .83em 0 }
        h4, p,
        blockquote, ul,
        fieldset, form,
        ol, dl, dir,
        menu            { margin: 1.12em 0 }
        h5              { font-size: .83em; margin: 1.5em 0 }
        h6              { font-size: .75em; margin: 1.67em 0 }
        h1, h2, h3, h4,
        h5, h6, b,
        strong          { font-weight: bolder }
        blockquote      { margin-left: 40px; margin-right: 40px }
        i, cite, em,
        var, address    { font-style: italic }
        pre, tt, code,
        kbd, samp       { font-family: monospace }
        pre             { white-space: pre }
        button, textarea,
        input, select   { display: inline-block }
        big             { font-size: 1.17em }
        small, sub, sup { font-size: .83em }
        sub             { vertical-align: sub }
        sup             { vertical-align: super }
        table           { border-spacing: 2px; }
        thead, tbody,
        tfoot           { vertical-align: middle }
        td, th, tr      { vertical-align: inherit }
        s, strike, del  { text-decoration: line-through }
        hr              { border: 1px inset }
        ol, ul, dir,
        menu, dd        { margin-left: 40px }
        ol              { list-style-type: decimal }
        ol ul, ul ol,
        ul ul, ol ol    { margin-top: 0; margin-bottom: 0 }
        u, ins          { text-decoration: underline }
        br:before       { content: "\A"; white-space: pre-line }
        center          { text-align: center }
        :link, :visited { text-decoration: underline }
        :focus          { outline: thin dotted invert }

        /* Begin bidirectionality settings (do not change) */
        BDO[DIR="ltr"]  { direction: ltr; unicode-bidi: bidi-override }
        BDO[DIR="rtl"]  { direction: rtl; unicode-bidi: bidi-override }

        *[DIR="ltr"]    { direction: ltr; unicode-bidi: embed }
        *[DIR="rtl"]    { direction: rtl; unicode-bidi: embed }

        @media print {
            h1            { page-break-before: always }
            h1, h2, h3,
            h4, h5, h6    { page-break-after: avoid }
            ul, ol, dl    { page-break-before: avoid }
        }

## Appendix C: Element Prioritization and Classification
All elements and attributes in HTMLite are optional for user agents to implement.
However this can make it harder to decide which elements a user agent wishes to implement first, so this appendix remedies that with a prioritized and categorized list.
User agents may decide that the optimal way for them to support a given element is to not treat it specially, which may undermine any use of the terms must, should, and may in this appendix.

### HTMLite-Core (must)
These are the absolutely core elements used to structure documents.

* `<html>`
* `<head>`
* `<base>`
* `<body>`
* `<p>`
* `<h1>`, `<h2>`, `<h3>`, `<h4>`, `<h5>`, & `<h6>`
* `<section>`
* `<a>`
* `<ol>`, `<ul>`, `<menu>`, & `<li>`
* `<br>`
* `<pre>`
* Fallback for invalid & unsupported elements.
* `@href`
* `@src` (May be treated same as `@href`, using `@alt` or `@title` for link text)

### HTMLite-Meta (should)
These are the elements which provides machine readable data which may be relevant to the user agent.

* `<title>`
* `<meta>`
* `<link>`
* `<data>`
* `<time>`
* `<noscript>`
* `@http-equiv`
* `@name`
* `@content`
* `@rel`
* `@type`
* `@charset`
* `@datetime`

### HTMLite-Text (should)
These tags provides basic semantic annotations for elements.

* `<em>`
* `<strong>`
* `<small>`
* `<i>`, `<b>`, `<u>`, & `<s>`
* `<dfn>` & `<abbr>`
* `<q>` & `<cite>`
* `<var>`, `<code>`, `<kbd>` & `<samp>`
* `<sub>` & `<sup>`
* `<del>` & `<ins>`
* `<mark>`
* `<blockquote>`
* `<dl>`, `<dt>`, & `<dd>`
* `<wbr>`
* `@title`
* `@cite`
* `@reversed` & `@start`

### HTMLite-Language (should)
These tags and attributes provides support for multilingual annotations.

* `<ruby>`
* `<rt>` & `<rp>`
* `<bdi>` & `<bdo>`
* `@lang`
* `@dir`
* `@translate`

### HTMLite-Table (should)
These tags and attributes facilitates creation of a tabular structure for information.

* `<table>`
* `<caption>`
* `<thead>`, `<tfoot>`, & `<tbody>`
* `<col>` & `<colgroup>`
* `<tr>`
* `<th>` & `<td>`
* `@span`, `@rowspan`, & `@colspan`
* `@headers` & `@scope`
* `@abbrev`

### HTMLite-Sections (may)
These tags and attributes add additional semantics to the paragraph-level structure of a document.

* `<div>` & `<span>`
* `<main>`
* `<header>`
* `<footer>`
* `<nav>`
* `<article>`
* `<aside>`
* `<address>`
* `<figure>` & `<figcaption>`

### HTMLite-Interactive (may)
These attributes and elements allows for documents to respond to certain actions without fetching additional data.

* `<details>`, `<summary>`, & `@open`
* `<meter>`, `@low`, `@high`, `@value`, & `@optimum`
* `@contextmenu`
* `@accesskey`
* `@tabindex`

### HTMLite-Forms (may)
These elements and attributes facilitates authors to solicit information from the reader, by having readers enter that information into "form controls".

* `<form>`, `@action`, `@method`, & `@enctype`
* `<input>`, `<textarea>`, & `<keygen>`
* `@name`, `@value`, `@type`, `@checked`, & `@form`
* `@challenge`, `@keytype`, & `@keyparams`/`@qpg`
* `<select>`, `<opgroup>`, `<option>`, `@label`, & `@selected`
* `@formaction`, `@formmethod`, `@formenctype`, & `@formtarget`

#### Labelling (must)
These elements and attribute associates human-readable names to form controls.

* `<fieldset>` & `<legend>`
* `<label>` & `@for`

#### Advanced (should)
These attributes enhance the presentation or interactivity of form controls.

* `@cols`, `@rows`, & `@wrap`
* `@accept` & `@capture`
* `@multiple`
* `@placeholder`
* `@spellcheck`
* `@size`
* `@autofocus`
* `@readonly` & `@disabled`

#### Autocomplete (may)
These attributes and element enhances user agents' abilities to suggest values readers might want to select for a form control.

* `@list` & `<datalist>`
* `@autocomplete`

#### Validation (may)
These attributes allows user agents to inform readers that the data being submitted would not be accepted by the destination webservice.

* `@novalidate` & `@formnovalidate`
* `@min`, `@max`, & `@step`
* `@minlength` & `@maxlength`
* `@pattern`
* `@required`

### HTMLite-Embedding (may)
These attributes and elements allows user agents to embed external resources, even if they're not richtext formats, into webpages.

* `@src` & `@alt`
* `@width` & `@height`
* `<iframe>`, `@srcdoc`, `@name`, & `@target`
* `<img>`
* `<video>` & `<audio>`
* `@controls`, `@loop`, & `@muted`
* `@poster` & `@preload`
* `<picture>` & `<source>`
* `<track>`
* `@default`, `@kind`

### HTMLite-Requests (may)
These attributes allows user agents to validate and/or tweak any external or embedded links.

* `@crossorigin`
* `@decoding`
* `@download`
* `@hreflang` & `@srclang`
* `@importance`
* `@integrity`
* `@loading`
* `@media`, `@sizes` & `@srcset`
* `@ping`
* `@referrerpolicy`
* `@sandbox`


### HTMLite-Style (may)
These attributes and elements allows authors to suggest how a user agent may denote the elements of a given document.

* `<style>` &`<link rel=stylesheet>`
* `@class` & `@id`
* `@style`

### HTMLite-Scrape (may)
These attributes aid programmatic tools in extracting data from webpages.

* `@itemscope` & `@itemtype`
* `@itemprop`

### HTMLite-ImageMaps (should not)
These attributes and elements allows readers to interact with documents by clicking specified locations on an image.

* `<map>` & `<area>`
* `@usemap`
* `@coords` & `@shape`
* `@ismap`

### WHATWG-reserved (should not)
These elements facilitates adding additional author-defined behaviour to documents. Associated attributes are not listed in this specification.

* `<script>`
* `<canvas>`
* `<dialog>`
* `<output>`
* `<portal>`
* `<progress>`
* `<slot>`
* `<template>`

## Annex D: Bibliography
* [BCP 14](https://www.rfc-editor.org/info/bcp14) Key Words for Use in RFCs to Indicate Requirement levels
* [ISO/IEC 14977 : 1996(E)](http://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf) Extended Backus–Naur form
* [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html) Date and Time Format
* [WHATWG HTML Living Standard](https://html.spec.whatwg.org/multipage/)
* [RFC 2045](http://www.ietf.org/rfc/rfc2045.txt) Multipurpose Internet Mail Extensions
* [RFC 2616](http://www.ietf.org/rfc/rfc2616.txt) Hypertext Transfer Protocol -- HTTP/1.1
* [RFC 3986](https://datatracker.ietf.org/doc/html/rfc3986/) Uniform Resource Identifier (URI): Generic Syntax
* [RFC 1467](https://www.rfc-editor.org/rfc/rfc1867.txt) Form-based File Upload in HTML
* [BCP 47](https://www.rfc-editor.org/info/bcp47) Language Tags
* [KDL](https://github.com/kdl-org/kdl/blob/main/SPEC.md)
* [XML in KDL](https://github.com/kdl-org/kdl/blob/main/XML-IN-KDL.md)
* [XMLite](https://codeberg.org/Weblite/XMLite/src/branch/master/XMLite.md)
* W3C's [HTML5 Entity Ref](https://dev.w3.org/html5/html-author/charref)
* W3C's [WebVTT](https://w3c.github.io/webvtt/)
* W3C's [CSS2.2](https://www.w3.org/TR/CSS22/)

### Registries
* [IANA Character Sets Registry](http://www.iana.org/assignments/character-sets/character-sets.xhtml)
* [ISO 639](https://www.iso.org/iso-639-language-codes.html) Language Codes
* [IANA Link Relations](https://www.iana.org/assignments/link-relations/link-relations.xhtml) Link Relations
* [WHATWG Link Types](https://html.spec.whatwg.org/multipage/links.html#linkTypes)
* [microformats wiki](https://microformats.org/wiki/existing-rel-values) Existing Rel Values
* [IANA Media Types](https://www.iana.org/assignments/media-types/media-types.xhtml)

### Historical
* [RFC1866](https://datatracker.ietf.org/doc/html/rfc1866) Hypertext Markup Language - 2.0
* [XHTML1](https://www.w3.org/TR/xhtml1/) XHTML 1.0 The Extensible HyperText Markup Language
